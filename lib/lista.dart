// import 'package:flutter/material.dart';

// List<Zbiur> zbiory = const <Zbiur>[
//   const Zbiur(
//     nazwa: 'Sorfy',
//     obrazek: 'lib/assets/sofa.png',
//   ),
//   const Zbiur(
//     nazwa: 'Fotele',
//     obrazek: 'lib/assets/fotel.png',
//   ),
//   const Zbiur(
//     nazwa: 'Stoły',
//     obrazek: 'lib/assets/stół.png',
//   ),
//   const Zbiur(
//     nazwa: 'Biurka',
//     obrazek: 'lib/assets/biurko.png',
//   ),
//   const Zbiur(
//     nazwa: 'Krzesła',
//     obrazek: 'lib/assets/krzesło.png',
//   ),
// ];

// class Zbiur {
//   final String nazwa;
//   final String obrazek;

//   const Zbiur({this.nazwa, this.obrazek});
// }

// class WidokZbiorow extends StatelessWidget {
//   final Zbiur zbiur;
//   final Zbiur pozycja;

//   const WidokZbiorow({Key key, this.zbiur, this.pozycja}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       color: Colors.white,
//       child: Column(
//         children: [
//           Container(
//             width: 300,
//             height: 400,
//             child: Image.asset(zbiur.obrazek),
//           ),
//           Text(
//             zbiur.nazwa,
//             style: Theme.of(context).textTheme.headline4,
//           )
//         ],
//       ),
//     );
//   }
// }

// class MyApp1 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         home: ListView(
//             scrollDirection: Axis.horizontal,
//             padding: const EdgeInsets.all(20.0),
//             shrinkWrap: true,
//             children: List.generate(
//               zbiory.length,
//               (index) {
//                 return Center(
//                   child: WidokZbiorow(
//                       zbiur: zbiory[index], pozycja: zbiory[index]),
//                 );
//               },
//             )));
//   }
// }
